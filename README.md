# Docker Compose Todo App

## Description
I wrote the code for this project while following along with the course [Deploy and run apps with Docker, Kubernetes, Helm and Rancher](https://www.udemy.com/course/deploy-and-run-apps-with-docker-kubernetes-helm-rancher). The code for the angular front end and the node backend were provided by the instructor. All of the containerization of the application I implemented. This project was a great chance for me to review a number of the fundamental concepts of Docker and Docker compose.

## Architecture
```plantuml
@startuml

folder "Clients Browser" as clients_browser{
    folder "Todo App" {

    }
}

node "Nginx Container" as todo_proxy {
    folder "Todo Proxy" {
        
    }
}

node "Node Container " as todo_frontend {
    folder "Todo Angular Frontend" {

    }
}

node "Node Container " as todo_api {
    folder "Todo NodeJs Rest API" {
        
    }
}

node "Todo-Elastic Container" as todo_elastic {
    database "Elastic Search" {
        folder "Todo Search DB" {
            
        }
    }
}

node "Todo-Postgres Container" as todo_postgres {
    database "PostgreSQL" {
        folder "Todo DB"  {
            
        }
    }
}

node "Todo-Redis Container" as todo_redis {
    database "Redis Cache" {
        folder "Todo Cache"   {
            
        }
    }
}

clients_browser -> todo_proxy
todo_proxy --> todo_frontend
todo_proxy --> todo_api
todo_api --> todo_postgres
todo_api --> todo_redis
todo_api --> todo_elastic



@enduml
```

## Local Setup
- **Run the app**: ```docker-compose up```
- **Stop the app**: ```docker-compose stop```
- **Rebuild the docker containers**: ```docker-compose up --build```

Once started the app will be available at http://localhost:8080.

# Notes from the course

## Docker Command Formats
**[docker] [run] [options] [image] [container commands]**

- **docker**  : Docker client  
- **run** : Docker command to start a new container  
- **options** : Docker command options  
- **image** : Docker image that the container should be based on
- **container** commands : Extra commands to send to the container

### Docker command options
- **-name** : Assigns a name to the container
- **-d** : Runs the container in detached mode
- **-i** : Keeps stdin open
- **-t** : Allocate a terminal 
- **-p** : Port maps
- **-v**: Volume maps

## Useful Docker Commands
- **docker container ls** : Lists running containers
- **docker container ls -a** : Lists stopped containers
- **docker inspect [container-name]** : Shows the container config
- **docker exec [container-name] env** : Prints environment variables for the container
- **docker run _--rm_ [image-name]** : --rm removes the container when it stops
- **docker run _-e_ developerName='ERIC-LAMMERS' [image-name]** : -e adds and environment variable to the container
- **docker container logs -f mynginx** : Tails the logs and prints them out
- **docker stop $(docker container ls -q)** : Stop all running containers
- **docker container ls -aq** : List stop containers ids
- **docker container ls -q** : List running containers ids

## Docker Networking
- **docker network create mynet** : Creates a docker network
- **docker network ls** : Lists out the docker networks
- **docker run -it --name [container-name] --net [network-name] [image-name] sh** : Containers running on the same network can access the other containers by their name. For example if the we have containers a and b then in b you can do: _ping a_)

## Dockerfile
- ** docker build . -f [docker-file-name] --tag [image-name]:[tag]** : Build a docker image with a specific tag 

## Docker Compose 
- Creates a network for free, the service name can be used for networking
- **docker-compose up --build** : Forces the images to be rebuilt

### Docker Volumes
- Can add named volumes for persistent storage even if the containers are removed ()
- **docker volume ls** : List out volumes
- **docker volume prune** : Cleans up anonymous volumes

### Why is docker compose not good for production?
- Only one node
- No Health Checks
- Load Balancing
- Scaling
- Service Discovery
- Secrets Management
- Application Templating
- Service Orchestrations

## Topics For Future Study
- Elastic Search / Kabana
- Nginx 
